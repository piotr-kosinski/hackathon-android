package com.example.piotrkosinski.myapplication;


import android.app.Activity;
import android.os.Bundle;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.view.View;

import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.loopj.android.http.*;
import cz.msebera.android.httpclient.Header;

import com.affectiva.android.affdex.sdk.Frame;
import com.affectiva.android.affdex.sdk.detector.CameraDetector;
import com.affectiva.android.affdex.sdk.detector.Detector;
import com.affectiva.android.affdex.sdk.detector.Face;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity implements Detector.ImageListener, CameraDetector.CameraEventListener {
    private WebView wv1;

    //CAMERA DETECTOR
    final String LOG_TAG = "Feelings Detector";
    final String API_URL = "http://192.168.210.204/mobile_api.php";
    final String EMAG_URL = "http://emag.ro";

    final String RECOGNIZE_FALSE = "I'm trying recognize yours face.";
    final String RECOGNIZE_TRUE = "I know what you feel now!";

    TextView statusTextView;

    RelativeLayout mainLayout;
    CameraDetector detector;
    SurfaceView cameraPreview;

    int previewWidth = 0;
    int previewHeight = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        statusTextView = (TextView) findViewById(R.id.statusTextView);

        wv1=(WebView)findViewById(R.id.webView);
        wv1.setWebViewClient(new MyBrowser());
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv1.loadUrl(EMAG_URL);

        // LAYOUT MODIFYING
        mainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        cameraPreview = new SurfaceView(this) {
            @Override
            public void onMeasure(int widthSpec, int heightSpec) {
                int measureWidth = MeasureSpec.getSize(widthSpec);
                int measureHeight = MeasureSpec.getSize(heightSpec);
                int width;
                int height;
                if (previewHeight == 0 || previewWidth == 0) {
                    width = measureWidth;
                    height = measureHeight;
                } else {
                    float viewAspectRatio = (float)measureWidth/measureHeight;
                    float cameraPreviewAspectRatio = (float) previewWidth/previewHeight;

                    if (cameraPreviewAspectRatio > viewAspectRatio) {
                        width = measureWidth;
                        height =(int) (measureWidth / cameraPreviewAspectRatio);
                    } else {
                        width = (int) (measureHeight * cameraPreviewAspectRatio);
                        height = measureHeight;
                    }
                }
                setMeasuredDimension(width,height);
            }
        };

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.CENTER_IN_PARENT,RelativeLayout.TRUE);
        cameraPreview.setLayoutParams(params);
        //cameraPreview.setAlpha(0);
        mainLayout.addView(cameraPreview,0);

        // CAMERA DETECTING
        detector = new CameraDetector(this, CameraDetector.CameraType.CAMERA_FRONT, cameraPreview);
        detector.setLicensePath("license.txt");
        detector.setDetectAllExpressions(true);
        detector.setDetectAllEmotions(true);
        detector.setDetectAllEmojis(true);
        detector.setDetectAllAppearances(true);
        detector.setImageListener(this);
        detector.setOnCameraEventListener(this);
        detector.start();
    }

    private class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onImageResults(List<Face> list, Frame frame, float v) {
        if (list == null)
            return;
        if (list.size() == 0) {
            statusTextView.setText(RECOGNIZE_FALSE);
        } else {
            Face face = list.get(0);
            Map Results = getFeelings(face);

            JSONObject resultsInJson = new JSONObject(Results);
            String resultsInString = resultsInJson.toString();
            statusTextView.setText(RECOGNIZE_TRUE);
            sendResults(resultsInString, wv1.getUrl());
        }
    }

    @SuppressWarnings("SuspiciousNameCombination")
    @Override
    public void onCameraSizeSelected(int width, int height, Frame.ROTATE rotate) {
        if (rotate == Frame.ROTATE.BY_90_CCW || rotate == Frame.ROTATE.BY_90_CW) {
            previewWidth = height;
            previewHeight = width;
        } else {
            previewHeight = height;
            previewWidth = width;
        }
        cameraPreview.requestLayout();
    }


    private Map getFeelings(Face face) {
        Map<String, Float> Results = new HashMap<String, Float>();
        Results.put("ANGER", face.emotions.getAnger());
        Results.put("CONTEMPT", face.emotions.getContempt());
        Results.put("DISGUST", face.emotions.getDisgust());
        Results.put("FEAR", face.emotions.getFear());
        Results.put("JOY", face.emotions.getJoy());
        Results.put("SADNESS", face.emotions.getSadness());
        Results.put("SURPRISE", face.emotions.getSurprise());
        Results.put("ATTENTION", face.expressions.getAttention());
        Results.put("BROW_FURROW", face.expressions.getBrowFurrow());
        Results.put("BROW_RAISE", face.expressions.getBrowRaise());
        Results.put("CHEEK_RAISE", face.expressions.getCheekRaise());
        Results.put("CHIN_RAISER", face.expressions.getChinRaise());
        Results.put("DIMPLER", face.expressions.getDimpler());
        Results.put("ENGAGEMENT", face.emotions.getEngagement());
        Results.put("EYE_CLOSURE", face.expressions.getEyeClosure());
        Results.put("EYE_WIDEN", face.expressions.getEyeWiden());
        Results.put("INNER_BROW_RAISER", face.expressions.getInnerBrowRaise());
        Results.put("JAW_DROP", face.expressions.getJawDrop());
        Results.put("LID_TIGHTEN", face.expressions.getLidTighten());
        Results.put("LIP_DEPRESSOR", face.expressions.getLipCornerDepressor());
        Results.put("LIP_PRESS", face.expressions.getLipPress());
        Results.put("LIP_PUCKER", face.expressions.getLipPucker());
        Results.put("LIP_STRETCH", face.expressions.getLipStretch());
        Results.put("LIP_SUCK", face.expressions.getLipSuck());
        Results.put("MOUTH_OPEN", face.expressions.getMouthOpen());
        Results.put("NOSE_WRINKLER", face.expressions.getNoseWrinkle());
        Results.put("SMILE", face.expressions.getSmile());
        Results.put("SMIRK", face.expressions.getSmirk());
        Results.put("UPPER_LIP_RAISER", face.expressions.getUpperLipRaise());
        Results.put("VALENCE", face.emotions.getValence());
        return Results;
    }

    private boolean sendResults(String resultsJson, String currentUrl) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("results", resultsJson);
        params.put("url", currentUrl);

        client.get(API_URL, params, new TextHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String res) {

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String res, Throwable t) {

                    }
                }
        );
        return true;
    }
}